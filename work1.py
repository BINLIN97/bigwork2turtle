
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from scipy.optimize import line_search




# 
def fx(x):
    return 10*((x[0]-1)**2)+(x[1]+1)**4


def gx(x):
    return np.array([20*x[0]-20,4*(x[1]+1)**3])


def hessianf(x):
    return np.array([[20., 0.],[0., 12*(x[1]+1)**2]], dtype=float)



# draw
def draw(xx, yy, cl):
    plt.plot(xx, yy, label='line 1', color=cl)
    return


# draw anim
def anim_line(points, cl):
    # line
    x, y = [], []
    n = 100
    for i in range(len(points)-1):
        x1, y1 = points[i]
        x2, y2 = points[i+1]
        x.extend([x1+j*(x2-x1)/n for j in range(n)])
        y.extend([y1+j*(y2-y1)/n for j in range(n)])
        draw([x1,x2], [y1,y2], cl)
    
    # anmi
    # x = np.array(x)
    # y = np.array(y)
    # def update_points(num):
    #     point_ani.set_data(x[num], y[num])
    #     return point_ani,
    # ani = animation.FuncAnimation(fig, update_points, np.arange(0, 100*len(points)), interval=0, blit=True)



# 牛顿法
def newton(step, x_init):
    color = 'red'
    print('>>>>>>>newton<<<<<<<')
    print('the %s line and points'%color)
    x = x_init.copy()
    plt.scatter(x[0], x[1], 10, color)
    points = [x]
    for i in range(step):
        # 偏导
        grandient = gx(x)
        # hessian矩阵
        hessian = hessianf(x)
        # hessian矩阵求逆
        inverse = np.linalg.inv(hessian)
        # iter
        x = x - np.matmul(inverse, grandient)

        plt.scatter(x[0], x[1], 10, color)
        points.append(x)

        print('step %s: x->[%s, %s], norm->%s'%(i, x[0], x[1], np.linalg.norm(grandient)))
        if np.linalg.norm(grandient) < ep:
            print('complete! use %s steps\n\n'%(1+i))
            break

    
    anim_line(points, color)
    return x, points


# gradient
def gradient(step, x):
    color = 'green'
    print('>>>>>>>gradient<<<<<<<')
    print('the %s line and points'%color)
    plt.scatter(x[0], x[1], 10, color)
    points = [x]
    for i in range(step):
        grandient = gx(x)
        d = -grandient
        alpha = line_search(fx, gx, x, d)[0]
        x = x + alpha*d
        plt.scatter(x[0], x[1], 10, color)
        points.append(x)
        print('step %s: x->[%s, %s], norm->%s'%(i, x[0], x[1], np.linalg.norm(grandient)))
        if np.linalg.norm(grandient) < ep:
            print('complete! use %s steps\n\n'%(1+i))
            break

    anim_line(points, color)
    return x, points


# cg
def cg_step(x_new, fx, step, preg, pred):
    # 偏导
    grandient = gx(x_new)
    if step==0:
        d = -grandient
    else:
        beta = np.matmul(grandient, grandient)/np.matmul(preg, preg)
        d = -grandient + beta*pred
    
    alpha = line_search(fx, gx, x_new, d)[0]
    # 迭代公式
    x_new = x_new + alpha*d
    return x_new, grandient, d


def cg(step, x):
    color = 'blue'
    print('>>>>>>>cg<<<<<<<')
    print('the %s line and points'%color)
    plt.scatter(x[0], x[1], 10, color)
    points = [x]
    gp, dp = 0, 0
    for i in range(step):
        x, gp, dp = cg_step(x, fx, i, gp, dp)
        plt.scatter(x[0], x[1], 10, color)
        points.append(x)
        print('step %s: x->[%s, %s], norm->%s'%(i, x[0], x[1], np.linalg.norm(gp)))
        if np.linalg.norm(gp) < ep:
            print('complete! use %s steps\n\n'%(1+i))
            break
    
    anim_line(points, color)
    return x, points


# bfgs
def bfgs_step(x_new, fx, step, preg, preH, prex):
    # 偏导
    grandient = gx(x_new)
    x_old = x_new.copy()
    if step==0:
        H = np.identity(2)
    else:
        dgk = grandient - preg
        dxk = x_new - prex
        H = np.matmul(np.matmul(np.eye(2) - np.matmul(dxk, dgk.T) / np.matmul(dgk.T, dxk), preH), np.eye(2) - np.matmul(dgk, dxk.T)/ np.matmul(dgk.T, dxk)) \
            + np.matmul(dxk, dxk.T) / np.matmul(dgk.T, dxk)

    d = -np.matmul(H, grandient)
    alpha = line_search(fx, gx, x_new, d)[0]
    # 迭代公式
    x_new = x_new + alpha*d    
    return x_new, grandient, H, x_old


def bfgs(step, x):
    color = 'black'
    print('>>>>>>>bfgs<<<<<<<')
    print('the %s line and points'%color)
    plt.scatter(x[0], x[1], 10, color)
    points = [x]

    gp, hp, xp = 0, 0, 0
    for i in range(step):
        x, gp, hp, xp = bfgs_step(x, fx, i, gp, hp, xp)
        plt.scatter(x[0], x[1], 10, color)
        points.append(x)
        print('step %s: x->[%s, %s], norm->%s'%(i, x[0], x[1], np.linalg.norm(gp)))
        if np.linalg.norm(gp) < ep:
            print('complete! use %s steps\n\n'%(1+i))
            break
    
    anim_line(points, color)
    return x, points



if __name__ == '__main__':
    x0 = np.array([0, 0], dtype=float)
    ep = 1e-4

    # draw
    fig = plt.figure(tight_layout=True)
    pxx, pyy = np.arange(-0.1,1.5,0.01), np.arange(-1.5,1,0.01)
    [px, py] = np.meshgrid(pxx, pyy)
    f = 10*((px-1)**2)+(py+1)**4
    plt.contour(px, py, f, 50)
    plt.grid(ls="--")

    # # newton
    _, points = newton(999, x0,)
    # gradient
    _, points = gradient(999, x0)
    # cg
    _, points = cg(9999, x0,)
    # bfgs
    _, points = bfgs(9999, x0)


    plt.show()
