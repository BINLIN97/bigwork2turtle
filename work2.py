import numpy as np

def fx(x):
    x = x[:,0]
    return (x[0]-2)**4 + (x[0]-2*x[1])**2

def hx(x):
    x = x[:,0]
    return x[0]**2 - x[1]

def gx(x):
    x = x[:,0]
    return np.array([4-x[0]**2-x[1]**2, x[0], x[1]]).T

   
def norm(x):
    return np.linalg.norm(x)
 
 
def constrains(x):
    x = x[:, 0]
    h=x[0]**2-x[1]
    g=[0,0,0]
    g[0]=4-x[0]**2-x[1]**2
    g[1]=x[0]
    g[2]=x[1]
    return h, np.array(g).T

 

def Lagrange(x,lamda,miu,c): 
    h, gk = constrains(x)
    fei = fx(x) + lamda*h + (c/2)*(h**2)
    for i in range(3):
        _x = min(0,miu[i] + c*gk[i])
        fei = fei + (1/(2*c))*((_x)**2 - miu[i]**2)
    return fei
 
               
def grad_fai(x,lamda,miu,c):
    g = [0,0]
    hh, gg = constrains(x)
    x = x[:,0]
    g[0] = 2*x[0] - 4*x[1] + 4*((x[0] - 2)**3) + 2*lamda*x[0] - 2*c*x[0]*(- x[0]**2 + x[1])
    g[1] = 8*x[1] - 4*x[0] - lamda + (c*(- 2*x[0]**2 + 2*x[1]))/2
    if miu[0] + c*gg[0] < 0:
        g[0] =g[0]-2*x[0]*(miu[0] - c*(x[0]**2 + x[1]**2 - 4))
        g[1] = g[1]-2*x[1]*(miu[0] - c*(x[0]**2 + x[1]**2 - 4))
    if miu[1] + c*gg[1] < 0:
        g[0] =g[0]+ miu[1] + c*x[0]
        g[1] = g[1]
    if miu[2] + c*gg[2] < 0:
        g[0] =g[0]
        g[1] = g[1]+miu[2] + c*x[1]
    return np.array([g])


def start_Lagrange(x0,eps):
    r=0.25
    c = 4
    a = 2
    k = 0
    lamda = 0
    miu = np.zeros([3,1])
 
    xk = get_xk(x0,lamda,miu,c)
    h0,g0 = constrains(xk)
    panduan = (h0**2+(min(g0[0],-miu[0]/c))**2+(min(g0[1],-miu[1]/c))**2+(min(g0[2],-miu[2]/c))**2)[0]
    while panduan > eps**2:
        h1,g1=constrains(xk)
        beita = norm(h1)/norm(h0)
        if beita > r:
            c = a*c;        
        for i in range(3):
            miu[i]= min(0,miu[i]+c*g1[i])
        lamda = lamda + c*h1
        print('step %s: %s'%(k,panduan))
        k = k+1
        x0 = xk
        xk = get_xk(x0,lamda,miu,c)
        
        h0,g0 =constrains(x0)
        panduan = (h0**2+(min(g0[0],-miu[0]/c))**2+(min(g0[1],-miu[1]/c))**2+(min(g0[2],-miu[2]/c))**2)[0]

    print('step %s: %s'%(k,panduan))
    print('complete!')
    print('x=[%s,%s]\nmin(f)=%s\n'%(xk[0,0],xk[1,0],fx(xk)))
    


def get_xk(x0,lamda,miu,c):
    n=2
    g0 = grad_fai(x0,lamda,miu,c)
    
    h0 = np.eye(2,2)
    s0 = -np.dot(h0,g0.T)
    
    k = 0
    count = 0
    lmd = wolfe_powell(x0,s0,lamda,miu,c)
    
    x1 = x0 +lmd*s0
    g1 = grad_fai(x1,lamda,miu,c)
    eps = 1e-6
    while (norm(g1) > eps):
        if k<n-1:
            detax = x1 - x0
            
            detag = g1.T - g0.T
            h1 = get_hk(h0,detax,detag)
            s1 = -np.dot(h1,g1.T)
            k = k+1
            
            x0 = x1
            g0 = grad_fai(x0,lamda,miu,c)
            s0 = s1
            h0 = h1
            lmd = wolfe_powell(x0,s0,lamda,miu,c)
            x1 = x0 +lmd*s0
            g1 = grad_fai(x1,lamda,miu,c)
        else:
            x0 = x1
            g0 = grad_fai(x0,lamda,miu,c)
            h0 = np.eye(2,2)
            s0 =  -np.dot(h0,g0.T)
            lmd = wolfe_powell(x0,s0,lamda,miu,c)
            x1 = x0 +lmd*s0
            g1 = grad_fai(x1,lamda,miu,c)
            k = 0

        count=count+1
    return x1


def wolfe_powell(xk,dk,lamda,miu,c):
    c1 = 0.1
    c2=0.5
    a = 0
    b =np.Inf
    lamda1 = 1
    while True:
        if not Lagrange(xk+lamda1*dk,lamda,miu,c)-Lagrange(xk,lamda,miu,c) \
            <= np.dot(c1*lamda1*grad_fai(xk,lamda,miu,c),dk)[0,0]:
            b = lamda1
            lamda1 = (lamda1 + a)/2
            continue

        if not (np.dot(grad_fai(xk+lamda1*dk,lamda,miu,c),dk) >= np.dot(c2*grad_fai(xk,lamda,miu,c),dk)):
            a = lamda1
            lamda1 = min(2*lamda1,(b+lamda1)/2)
            continue


        break
    return lamda1

 
def get_hk(h,x,g):
    miu = (1 + np.dot(np.dot(g.T,h),g)/np.dot(x.T,g))[0,0]
    fenzi = np.dot(np.dot(miu,x),x.T)-np.dot(np.dot(h,g),x.T)-np.dot(np.dot(x,g.T),h)
    hk = h + fenzi/np.dot(x.T,g)
    return hk


if __name__ == '__main__':
    x = np.array([[0], [0]])
    eps = 1e-4
    start_Lagrange(x,eps)